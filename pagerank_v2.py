from func_module import *
import matplotlib.pyplot as plt

print("------------------------------------------")
##############################################
#           1. Adjacency matrix              #
##############################################
A = np.array([[0, 0, 1, 0, 0, 0],
              [1, 0, 1, 0, 0, 0],
              [1, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 1, 1],
              [0, 0, 1, 1, 0, 0],
              [0, 0, 0, 1, 1, 0]])
##############################################
#            2. Stochastic matrix            #
##############################################
S = Stochastic_Mat(A)
##############################################
#       3. Google matrix                     #
#       4. The pageRank classification       #
##############################################
alpha = np.arange(0.5,1,0.01)       # An array of 50 values for alpha
P_initial = 1./6.* np.ones((6,1))   # Initial P to be used in power iteration algorithm

P_ls = []    # A list containing iteration values of probabilities for all alpha values
P_final_ls = []    # A list containing final values of probabilities for all alpha values
Page_Ranks_ls = []    # A list containing ranks of nodes of the final probabilities

eps = 1e-6

for item in alpha:
    G = Google_Mat(S, item)           # Google matrix
    P = Power_Iteration(P_initial, G) # Calculation of probability distribution
    P_ls.append(P)

    P_final = np.transpose(P[-1])[0]  # The last prob. value
    P_final_ls.append(P_final)

    Page_Ranks = Rank(P,G)            # all the ranks at each iteration
    Page_Ranks_ls.append(Page_Ranks)

I = np.arange(1,7,1)
# #######################################################
# #   Barchart of probability dist. func. (alpha=0.85)  #
# #######################################################
fig1 = plt.figure()
plt.bar(I, P_final_ls[7])
plt.title(r'Probability distribution ($\alpha=0.85$)', fontsize=18)
plt.xticks(I, ['Node 1', 'Node 2', 'Node 3', 'Node 4', 'Node 5', 'Node 6'], fontsize=14)
plt.ylabel('Probability', fontsize=14)

def BarChart_Probability(fig1, I, P_final_ls, Alpha):
    fig1 = plt.figure()
    plt.bar(I, P_final_ls[7])
    plt.title(r'Probability distribution ($\alpha=Alpha$)', fontsize=18)
    plt.xticks(I, ['Node 1', 'Node 2', 'Node 3', 'Node 4', 'Node 5', 'Node 6'], fontsize=14)
    plt.ylabel('Probability', fontsize=14)

BarChart_Probability(fig1, I, P_final_ls[7], 0.85)
##########################################################
#   Barchart of probability dist. func. (alpha=[0.5,1))  #
##########################################################
fig2 = plt.figure()
plt.plot(alpha, P_final_ls)
plt.title('Probability distribution of nodes', fontsize=18)
plt.ylabel('Probability', fontsize=14)
plt.xlabel('Alpha', fontsize=14)
plt.legend(['Node 1', 'Node 2', 'Node 3', 'Node 4', 'Node 5', 'Node 6'])

############################
#    degree centrality     #
############################
InDeg = Degree_Centrality(A)[0]    #The number of links going to the nodes
OutDeg = Degree_Centrality(A)[1]    #The number of links going out of the nodes
Deg = (InDeg[:] + OutDeg[:])/sum((InDeg[:] + OutDeg[:]))    #The centrality of each node

fig3 = plt.figure()
plt.bar(I-0.17, Deg, 0.3, label='Degree centrality')
plt.bar(I+0.17, P_final_ls[7], 0.3, label='PageRank centrality')
plt.title('Probability distribution of the centrality for degree and PageRank methods', fontsize=18)
plt.xticks(I, ['Node 1', 'Node 2', 'Node 3', 'Node 4', 'Node 5', 'Node 6'], fontsize=14)
plt.ylabel('Probability', fontsize=14)
plt.legend()

##############################################
#               5. The Chei-Rank             #
##############################################
A_tr = A.transpose()
S_tr = Stochastic_Mat(A_tr)
G_ch = Google_Mat(S_tr, alpha[7])         # Google matrix
P_ch = Power_Iteration(P_initial, G_ch)   # Calculation of probability distribution
P_ch_final = np.transpose(P_ch[-1])[0]    # The last prob. value
Ch_Ranks = Rank(P_ch, G_ch)    #An array containing arrays of ranks at all iterations

P_ch_2 = np.zeros((len(Ch_Ranks), 6))
for i in range(len(Ch_Ranks)):
    P_ch_2[i] = np.transpose(P_ch[i])[0]

#######################################################
#   Barchart of probability dist. func. (alpha=0.85)  #
#######################################################
fig4, ax4 = plt.subplots()
ax4.bar(I, P_ch_final)
plt.title(r'rrrProbability distribution with CheiRank method ($\alpha=0.85$)', fontsize=18)
plt.xticks(I, ['Node 1', 'Node 2', 'Node 3', 'Node 4', 'Node 5', 'Node 6'], fontsize=14)
plt.ylabel('Probability', fontsize=14)
# ############################
# #    Animated histogram    #
# ############################
# for i in range(len(P_ch_2)):
#     plt.clf()
#     plt.bar(I,P_ch_2[i])
#     plt.ylabel('Probability')
#     plt.title("CheiRank vector as a function of iteration")
#     plt.pause(0.05)

plt.show()