from func_module import *
import matplotlib.pyplot as plt
import networkx as nx

File1 = "nodes.txt"
# Read the "id" and "name" values from the file "nodes.txt"
f1   = np.loadtxt(File1, usecols=(0, 1), skiprows=1, dtype = str)
ID   = f1[:,0].astype(int)
Name = f1[:,1].astype(str)

File2 = "links.txt"
# Read the "attack", "defence" and "efficaciate" values from the file "links.txt"
f2 = np.loadtxt(File2, usecols=(0, 1, 2), skiprows=1)
Attack = f2[:,0].astype(int)
Defence = f2[:,1].astype(int)
Efficaciate = f2[:,2].astype(float)

# Making the adjacency matrix
N = len(ID)
A = np.ones((N,N))

A[Attack[:]-1, Defence[:]-1] = Efficaciate[:]
A = np.transpose(A)

# Making the stochastic matrix
S = Stochastic_Mat(A)

P_initial = 1./N * np.ones((N,1))
alpha = 0.85

##############################################
#                  PageRank                  #
##############################################
G = Google_Mat(S, alpha)            # The Google matrix of the network
P = Power_Iteration(P_initial, G)   # Th probability distribution of the network (using power iteration algorithm)
P_final = np.transpose(P[-1])       # The last probability distribution
PR = Rank(P_final,G)[-1]            # An array in which PR[i] gives the node of rank i

# Making an array containing the ranks
Page_Ranks = np.zeros(N)
for i in range(N):
    Page_Ranks[i] = np.where(PR == i+1)[0][0] +1
Page_Ranks = Page_Ranks.astype(int) # An array in which Page_Ranks[i] gives the rank of the node i+1

I = np.arange(1,N+1)                # An array denoting the nodes

# The probability distribution of the network (using the diagonalization algorithm)
val, vec = np.linalg.eig(G)
P = np.abs(vec[:,0])/sum(np.abs(vec[:,0]))

# Writing the PageRanks in a text file
with open("./PageRanks.txt", "w+") as f2:
    f2.write('{} {}\n'.format(EPS,N))
    f2.write('\n')
    f2.write('Rank - ID - TYPE - PageRank (power interation) - PageRank (diagonalization)\n')
    for i in range(N):
        f2.write('{}-{}-{}-{}-{}\n'.format(int(Page_Ranks[i]), I[i],
        Name[i], P_final[0][i], P[i]))

# Plotting the difference in probability distributions found
# using diagonalization and the power iteration algorithms
diff = np.abs(P_final[0]-P)
fig1 = plt.figure()
plt.plot(I, diff, label='Diagonalization')
plt.title('Power iteration and diagonalization probability differences ($\epsilon={}$)'.format(EPS), fontsize=17)
plt.xticks(I, fontsize=14)
plt.ylabel('$|P_{Power Iteration} - P_{diagonalization}|$', fontsize=14)
plt.xlabel('Nodes', fontsize=14)

##############################################
#                  CheiRank                  #
##############################################
C_initial = 1./N * np.ones((N,1))       # Initial probability distributions
A_tr = A.transpose()                    # Adjacency matrix
S_tr = Stochastic_Mat(A_tr)             # Stochastic matrix
G_ch = Google_Mat(S_tr, alpha)          # The Google matrix of the network
C    = Power_Iteration(C_initial, G_ch) # Th probability distribution of the network
C_final = np.transpose(C[-1])           # The last probability distribution
CR = Rank(C_final,G)[-1]                # An array in which PR[i] gives the node of rank i
# Making an array containing the ranks
Chei_Ranks = np.zeros(N)
for i in range(N):
    Chei_Ranks[i] = np.where(CR == i+1)[0][0] +1
Chei_Ranks = Chei_Ranks.astype(int) # An array in which Page_Ranks[i] gives the rank of the node i+1

# The probability distribution of the network (using the diagonalization algorithm)
val_ch, vec_ch = np.linalg.eig(G_ch)
P_ch = np.abs(vec_ch[:,0])/sum(np.abs(vec_ch[:,0]))

# Writing the CheiRanks in a text file
with open("./CheiRanks.txt", "w+") as f3:
    f3.write('{} {}\n'.format(EPS,N))
    f3.write('\n')
    f3.write('Rank - ID - TYPE - CheiRank (power interation) - CheiRank (diagonalization)\n')
    for i in range(N):
        f3.write('{}-{}-{}-{}-{}\n'.format(int(Chei_Ranks[i]), I[i],
         Name[i], C_final[0][i], P_ch[i]))

# Plotting the difference in probability distributions found
# using diagonalization and the power iteration algorithms
diff_ch = np.abs(C_final[0]-P_ch)
fig2 = plt.figure()
plt.plot(I, diff_ch, label='Diagonalization')
plt.title('Power iteration and diagonalization probability differences ($\epsilon={}$)'.format(EPS), fontsize=17)
plt.xticks(I, fontsize=14)
plt.ylabel('$|P_{Power Iteration} - P_{diagonalization}|$', fontsize=14)
plt.xlabel('Nodes', fontsize=14)

############################
#    Bonus                 #
############################
fig3 = plt.figure()
link = np.array([Attack, Defence])
a = Attack[ : , np.newaxis]
d = Defence[ : , np.newaxis]
np.savetxt('links2.txt', np.hstack([a, d]), fmt=['%i', '%i'])

Labels = dict(zip(ID, Name))
print(Labels)
graph = nx.read_edgelist('links2.txt')
# print(nx.info(graph))

nx.draw(graph, node_color='b', edge_color='g', edge_cmap=plt.cm.Blues, width=2, with_labels=True)

plt.show()