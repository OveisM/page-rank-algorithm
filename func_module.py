import numpy as np

EPS = 1e-6

def Stochastic_Mat(Mat):
    '''
    DO: This function calculates stochastic matrix of the nodes of a network.

    ---INPUT PARAMETERS---

    Mat: The matrix of the links between nodes of a network. A[i,j]=1 if
    there is link fromnode j to node i. Otherwise, A[i,j]=0

    ---OUTPUT PARAMETERS---

    S_Mat: The output is the stochastic matrix
    '''
    N = len(Mat)
    k = np.zeros(N)
    for j in range(N):
        k[j] = sum(Mat[:,j])

    S_Mat = np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            if k[j]!=0:
                S_Mat[i,j] = Mat[i,j]/k[j]
            else:
                S_Mat[i,j] = 1/N
    return S_Mat

def Google_Mat(S_Mat, Alpha):
    '''
    DO: This function calculates the google matrix.

    ---INPUT PARAMETERS---

    S_Mat: The stochastic matrix of the dimension N*N of a network
    Alpha: The damping factor, which is a float number.

    ---OUTPUT PARAMETERS---

    G_MAT: The Google matrix of dimension N*N
    '''
    N = len(S_Mat)
    G_MAT = np.zeros((N,N))
    G_MAT[:,:] = Alpha*S_Mat[:,:] + (1.- Alpha) * 1./N
    return G_MAT

def Power_Iteration(P_init, G_MAT):
    '''
    DO: This function finds the PageRank vector P. The ith component of P is
    proportional to the number of times the random surfer reach node i.
    
    ---INPUT PARAMETERS---
    
    P_init: A vector of with length equal the number of nodes in the network
    and each elements a float number showing the degree of each nodes at the begining
    of the random walk process

    G_MAT: the google matrix of the network of N nodes, with N*N float values

    ---OUTPUT PARAMETERS---
    
    P_all: An array containing arrays of probabilities of the nodes after each iteration
    '''
    P = P_init
    P_all=[]
    P_all.append(P)
    n = 0
    while np.all(np.matmul(G_MAT,P)-P<=EPS)!=True:
        P = np.matmul(G_MAT,P)
        P_all.append(P)
        n+=1
    P_all = np.array(P_all)
    return P_all


def Rank(P_MAT, G_MAT):
    '''
    Calculates the ranking of nodes
    
    --- INPUTS ---
    
    P_MAT: A matrix containing the probabilities of all nodes.

    G_MAT: The google matrix of the network

    --- OUTPUT ---

    PR: A matrix containing the ranks after each iteration in power iteration.
    '''
    PR=[]
    for i in range(len(P_MAT)):
        N = len(G_MAT)
        P_list = []
        for j in range(N):
            P_list.append(P_MAT[i][j])
        Sorted = []
        Index  = []
        for j in range(N):
            Sorted.append(max(P_list))
            Index.append(P_list.index(max(P_list))+1)
            P_list[P_list.index(max(P_list))] = 0
        PR.append(np.array(Index))
    return PR

def Degree_Centrality(A_Mat):
    '''
    DO: This function calculates the in-degree and out-degree centrality.

    ---INPUT PARAMETERS---

    A_Mat: The adjacency matrix of the network

    ---OUTPUT PARAMETERS---

    The output is a tuple containing two arrays

    In_Degree: The in-degree values of each node
    Out_Degree: The out-degree values of each node
    '''
    n=len(A_Mat)
    A_Mat_Transpose = A_Mat.transpose()
    In_Degree=n*[0]
    Out_Degree=n*[0]
    for i in range(6):
        for j in range(6):
            if A_Mat[i,j]==1:
                In_Degree[i]+=1
            if A_Mat_Transpose[i,j]==1:
                Out_Degree[i]+=1
    In_Degree = np.array(In_Degree)
    Out_Degree = np.array(Out_Degree)

    return In_Degree, Out_Degree